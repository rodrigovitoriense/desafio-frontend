import { AppProps } from 'next/app'
import Head from 'next/head';
import React from 'react';
import { GlobalStorage } from '../context/GlobalStorage';
import '../styles/styles.css';

function MyApp({ Component, pageProps }: AppProps) {
  return (
      <>
      <Head>
        <title>Desafio Frontend</title>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
      </Head>
        <GlobalStorage>
          <Component {...pageProps} />
        </GlobalStorage>
      </>

  );
}

export default MyApp
