import React from "react";
import { Header } from "../../components/Header";
import { Footer } from "../../components/Footer";
import {ListUsers} from '../../components/ListUsers'

export default function results() {
  return(
    <>
      <Header />
      <ListUsers />
      <Footer />
    </>
  );
}