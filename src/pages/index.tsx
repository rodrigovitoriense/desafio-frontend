
import React from "react";
import { Footer } from "../components/Footer";
import { Header } from "../components/Header";
import { LogoHome } from "../components/LogoHome";


export default function Home() {
  return (
    <>
      <Header/>
      <LogoHome />
      <Footer/>
    </>
  )
}
