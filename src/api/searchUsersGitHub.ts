import { convertDate } from "../utils/convertDate";

const URL_API_SearchUsers = "https://api.github.com/search/users?q=";
const URL_API_GetUser = "https://api.github.com/users/";

async function getDetailsUser(loginUser: string) {
  try {
    const fetchUser = await fetch(`${URL_API_GetUser}${loginUser}`, {
      method: "GET",
      headers: {
        Authorization: `token ${process.env.API_TOKEN} `,
      },
    });
    const dataUser = await fetchUser.json();
    return {
      name: dataUser.name,
      followers: dataUser.followers,
      following: dataUser.following,
      created_at: convertDate(dataUser.created_at),
    };
  } catch {
    console.log("Erro de conexão com a API");
    return;
  }
}

export const searchUsersGitHub = async (nameUser: string) => {
  const fetchSearchUsers = await fetch(URL_API_SearchUsers + nameUser);
  const dataSearchUsers = await fetchSearchUsers.json();

  const usersInformations = await Promise.all(
    dataSearchUsers.items.map(async (user) => {
      const userDetails = await getDetailsUser(user.login);

      return {
        login: user.login,
        id: user.id,
        avatar_url: user.avatar_url,
        url: user.html_url,
        score: user.score,
        ...userDetails,
      };
    })
  );
  return usersInformations;
};
