export function convertDate (date:string) {
  const dateCut = date.substring(10,-1);
  return dateCut.split('-').reverse().join('/');

}
