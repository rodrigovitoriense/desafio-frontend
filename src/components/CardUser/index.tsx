import {
  Card,
  Button,
  CardMedia,
  CardContent,
  Typography,
  Link,
} from "@mui/material";

import { useState } from "react";

import { ModalSeeMore } from "../ModalSeeMore";

import { makeStyles } from "@mui/styles";

interface UserProps {
  login: string;
  id: number;
  avatar_url: string;
  url: string;
  score: number;
  name: string;
  followers: number;
  following: number;
  created_at: string;
}

const useStyles = makeStyles({
  root: {
    height: "273px",
    width: "230px",
    borderRadius: "10px",
    boxShadow: "0px 0px 6px var(--gray-350)",

    "@media(max-width: 720px)": {
      textAlign: "center",
    },

    "& img": {
      height: "130px",
      objectFit: "fill",
    },
  },
  content: {
    display: "flex",
    flexDirection: "column",
    gap: "0.25rem",
    padding: "0.625rem 0.625rem 0",
  },
  nameUser: {
    fontSize: "1.125rem",
    fontWeight: "400",
    lineHeight: "1.318125rem",
    color: "var(--purple-200)",
  },
  link: {
    fontSize: "0.75rem",
    lineHeight: "0.87875rem",
    color: "var(--blue-100)",
    textDecoration: "none",

    "&:hover": {
      filter: "brightness(0.8)",
    },
  },
  score: {
    fontSize: "0.75rem",
    lineHeight: "0.87875rem",
    color: "var(--purple-300)",
  },
  button: {
    width: "100%",
    padding: "1.125rem 0.625rem",

    "& button": {
      width: "100%",
      background: "var(--purple-500)",
      color: "var(--white)",
      borderRadius: "1.25rem",
      fontSize: "0.875rem",
      fontWeight: "500",
      lineHeight: "1.025625rem",
      padding: "0.625rem",

      "&:hover": {
        background: "var(--purple-500)",
        filter: "brightness(0.8)",
      },
    },
  },
});

export function CardUser(props: UserProps) {
  const classes = useStyles();

  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <>
      <Card className={classes.root}>
        <CardMedia
          component="img"
          image={props.avatar_url}
          alt={"Foto do " + props.name}
        />
        <CardContent className={classes.content}>
          <Typography component="h3" className={classes.nameUser}>
            {props.name || props.login}
          </Typography>
          <Link
            href={props.url}
            variant="body2"
            className={classes.link}
            target="_blank"
          >
            {props.url}
          </Link>
          <Typography variant="body2" className={classes.score}>
            Score: {props.score.toFixed(2)}
          </Typography>
        </CardContent>
        <div className={classes.button}>
          <Button variant="contained" onClick={handleOpen}>
            VER MAIS
          </Button>
        </div>
      </Card>

      <ModalSeeMore open={open} close={handleClose} {...props} />
    </>
  );
}
