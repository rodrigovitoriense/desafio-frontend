import React from "react";

import { TextField, IconButton, InputAdornment } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";

import { useRouter } from "next/router";
import Link from "next/link";
import { useContext } from "react";

import { GlobalContext } from "../../context/GlobalStorage";
import { searchUsersGitHub } from "../../api/searchUsersGitHub";

import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  root: {
    background: "var(--white)",
    height: "3.5rem",
    padding: "0.625rem 2.5rem",
    boxShadow: "0px 4px 4px var(--gray-300)",
    display: "flex",
    alignItems: "center",
    gap: "2rem",
    zIndex: 2000,
    position: "sticky",

    "& form": {
      width: "100%",
    },
  },

  logo: {
    height: "100%",
    cursor: "pointer",
    "& img": {
      height: "100%",
    },
  },
  inputSearch: {
    lineHeight: "0,87875rem",
    height: "2.25rem",

    "& input": {
      fontSize: "0.75rem",
    },
    "& label": {
      fontSize: "0.75rem",
    },

    "& .MuiOutlinedInput-root": {
      height: "100%",
    },

    "& button": {
      padding: 0,
      color: "var(--white)",
      background: "var(--purple-500)",
      position: "absolute",
      right: 0,
      top: 0,
      bottom: 0,
      width: "2.25rem",
      boxShadow: "-2px 0px 4px rgba(0, 0, 0, 0.25)",
      borderRadius: "0px 4px 4px 0px",

      "&:hover": {
        background: "var(--purple-500)",
        filter: "brightness(0.8)",
      },
      "& svg": {
        width: "1.25rem",
        height: "1.25rem",
      },
    },
  },
});

export function Header() {
  const classes = useStyles();
  const router = useRouter();
  const context = useContext(GlobalContext);

  async function handleSubmit(e) {
    e.preventDefault();

    if (context.userSearch.trim() !== "") {
      const listUsers = await searchUsersGitHub(context.userSearch);
      context.setListUsers([...listUsers]);
      router.push("/results");
      context.setLastUserSearch(context.userSearch);
    } else {
      alert("Preencha o campo de pesquisa.");
      context.setUserSearch("");
    }
  }
  return (
    <header className={classes.root}>
      <div className={classes.logo}>
        <Link href="/">
          <img
            src="small-logo.svg"
            alt="Logo Git Search e Home"
            onClick={() => context.setUserSearch("")}
          />
        </Link>
      </div>

      <form onSubmit={handleSubmit}>
        <TextField
          className={classes.inputSearch}
          label="Pesquisar"
          size="small"
          fullWidth
          value={context.userSearch}
          onChange={(e: any) => context.setUserSearch(e.target.value)}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton onClick={handleSubmit}>
                  <SearchIcon />
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </form>
    </header>
  );
}
