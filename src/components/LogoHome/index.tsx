import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "100vw",
    height: "calc(100vh - 3.5rem - 2.25rem)",
  },
  bigLogo: {
    height: "410px",
    width: "730px",
    padding: "1rem",
    "& img": {
      height: "auto",
      width: "100%",
    },
  },
});

export function LogoHome() {
  const classes = useStyles();
  return (
    <>
      <main className={classes.root}>
        <div className={classes.bigLogo}>
          <img src="big-logo.svg" alt="Big logo" />
        </div>
      </main>
    </>
  );
}
