import { Box, Typography, Grid } from "@mui/material";

import { useContext } from "react";
import Head from "next/head";

import { GlobalContext } from "../../context/GlobalStorage";
import { CardUser } from "../CardUser";

import { makeStyles } from "@mui/styles";

interface UserProps {
  login: string;
  id: number;
  avatar_url: string;
  url: string;
  score: number;
  name: string;
  followers: number;
  following: number;
  created_at: string;
}

const useStyles = makeStyles({
  root: {
    width: "100vw",
    height: "calc(100vh - 3.5rem - 2.25rem)",
    display: "flex",
    justifyContent: "center",
    padding: "2.5rem 1rem",
    zIndex: 1000,
    position: "relative",
    overflow: "auto",
  },
  containerResults: {
    maxWidth: "980px",
  },
  titleResults: {
    padding: "0.5625rem 0 0.625rem 0",
    borderBottom: "1px solid var(--gray-50)",
    marginBottom: "1.25rem",
    fontSize: "1.125rem",
    fontWeight: "400",
    lineHeight: "1.318125rem",
  },
});

export function ListUsers() {
  const classes = useStyles();

  const context = useContext(GlobalContext);

  return (
    <>
      <Head>
        <style>
          {
            'body {  position: relative;overflow-x: hidden;} body:before { content: "";display: block;position: absolute;left: 0;top: 0;width: 100%;height: 100%;z-index: 1;opacity: 0.2;background-image: url("bg-results.svg");background-repeat: no-repeat;background-size: cover;background-position: 50% 0;}'
          }
        </style>
      </Head>
      <main className={classes.root}>
        <section className={classes.containerResults}>
          <Box>
            <Typography className={classes.titleResults}>
              Resultados para: {context.lastUserSearch}
            </Typography>
          </Box>
          <Grid
            container
            spacing={2.5}
            sx={{ flexDirection: { xs: "column", sm: "row" } }}
          >
            {context.listUsers &&
              context.listUsers.map((props: UserProps) => {
                return (
                  <Grid item xs={12} sm={6} md={4} lg={3} key={props.id}>
                    <CardUser {...props} />
                  </Grid>
                );
              })}
          </Grid>
        </section>
      </main>
    </>
  );
}
