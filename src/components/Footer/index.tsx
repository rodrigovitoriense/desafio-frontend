import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  root: {
    background: "var(--white)",
    height: "2.25rem",
    padding: "0.625rem 0",
    boxShadow: "0px -4px 4px var(--gray-300)",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: "var(--purple-300)",
    position: "fixed",
    left: 0,
    bottom: 0,
    width: "100%",
    zIndex: 2000,

    "& p": {
      fontSize: "0.875rem",
    },
  },
});
export function Footer() {
  const classes = useStyles();
  return (
    <footer className={classes.root}>
      <p> Projetado por Rodrigo Vitoriense - 24/12/2021</p>
    </footer>
  );
}
