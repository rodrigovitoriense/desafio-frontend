import {
  Typography,
  CardMedia,
  CardContent,
  Card,
  Button,
  Box,
  Modal,
  Link,
} from "@mui/material";

import { makeStyles } from "@mui/styles";

interface ModalProps {
  login: string;
  id: number;
  avatar_url: string;
  url: string;
  score: number;
  name: string;
  followers: number;
  following: number;
  created_at: string;
  open: boolean;
  close: () => void;
}

const useStyles = makeStyles({
  root: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",

    width: "980px",
    height: "346px",
    padding: "2.5rem",
    background: "var(--white)",
    borderRadius: "0.625rem",
    boxShadow: "0px 0px 6px var(--gray-350)",

    display: "flex",
    gap: "1.25rem",

    "@media(max-width: 1000px)": {
      width: "680px",
      height: "346px",
    },
    "@media(max-width: 720px)": {
      height: "80vh",
      width: "350px",
      flexDirection: "column",
      padding: "0",
    },

    "& img": {
      maxWidth: "266px",
      maxHeight: "266px",
      borderRadius: "0.625rem",

      "@media(max-width: 720px)": {
        maxWidth: "100%",
        maxHeight: "40%",
        objectFit: "fill",
      },
    },
  },
  cardContent: {
    width: "100%",
    "& h3": {
      fontSize: "1.125rem",
      fontWeight: "400",
      lineHeight: "1.318125rem",
      padding: "0.5625rem 0 0.625rem 0.125rem",
      borderBottom: "1px solid var(--gray-50)",
      color: "var(--purple-200)",
    },
  },
  details: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: "1.25rem",

    "& label , & p": {
      fontSize: "0.875rem",
      fontWeight: "400",
      lineHeight: "1.025625rem",
    },
    "& label": {
      color: "var(--purple-200)",
    },
    "& p, & a": {
      color: "var(--purple-300)",
    },
    "& a": {
      textDecoration: "none",
    },
  },
  detailsColumns: {
    display: "flex",
    flexDirection: "column",
    gap: "1.25rem",

    "& article label": {
      marginBottom: "0.25rem",
    },

    "&:last-child": {
      "& article": {
        textAlign: "right",
      },
    },
  },
  button: {
    fontSize: "0.875rem",
    fontWeight: "500",
    color: "var(--purple-500)",
    padding: "0.625rem 1.0625rem",
    borderRadius: "0.25rem",
    border: "1px solid var(--purple-500)",
    marginTop: "2.5rem",
    lineHeight: "1.025625rem",
    letterSpacing: "0",

    "&:hover": {
      filter: "brightness(0.8)",
    },

    "@media(max-width: 720px)": {
      marginTop: "3.5rem",
    },
  },
});

export function ModalSeeMore(props: ModalProps) {
  const classes = useStyles();

  return (
    <>
      <Modal
        open={props.open}
        onClose={props.close}
        aria-labelledby="modal-see-more"
        aria-describedby="modal see more informations about user."
      >
        <Card className={classes.root}>
          <CardMedia
            component="img"
            image={props.avatar_url}
            alt={"Foto de " + props.name}
          />
          <CardContent className={classes.cardContent}>
            <Typography gutterBottom component="h3">
              {props.name || props.login}
            </Typography>

            <Box className={classes.details}>
              <Box className={classes.detailsColumns}>
                <article>
                  <Typography component="label">Username:</Typography>
                  <Typography component="p">{props.login}</Typography>
                </article>
                <article>
                  <Typography component="label">Cadastrado(a):</Typography>
                  <Typography component="p">{props.created_at}</Typography>
                </article>

                <article>
                  <Typography component="label">URL:</Typography>
                  <Typography component="p">
                    <Link href={props.url} target="_blank">
                      {props.url}
                    </Link>
                  </Typography>
                </article>
              </Box>

              <Box className={classes.detailsColumns}>
                <article>
                  <Typography component="label">Seguindo:</Typography>
                  <Typography component="p">{props.following}</Typography>
                </article>
                <article>
                  <Typography component="label">Seguidores:</Typography>
                  <Typography component="p">{props.followers}</Typography>
                </article>
                <article>
                  <Button className={classes.button} onClick={props.close}>
                    Fechar
                  </Button>
                </article>
              </Box>
            </Box>
          </CardContent>
        </Card>
      </Modal>
    </>
  );
}
