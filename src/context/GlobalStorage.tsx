import { createContext, ReactNode, useState } from "react";

interface UserProps {
  login: string;
  id: number;
  avatar_url: string;
  url: string;
  score: number;
  name: string;
  followers: number;
  following: number;
  created_at: string;
}

interface ResultContext {
  userSearch: string;
  setUserSearch: (userSearch: string) => void;
  lastUserSearch: string;
  setLastUserSearch: (userSearch: string) => void;
  listUsers: Array<UserProps> | Array<{}>;
  setListUsers: (listUsers: Array<UserProps> | Array<{}>) => void;
}

interface ContextPropsType {
  children: ReactNode;
}

export const GlobalContext = createContext({} as ResultContext);

export const GlobalStorage = (props: ContextPropsType) => {
  const [userSearch, setUserSearch] = useState("");
  const [lastUserSearch, setLastUserSearch] = useState("");
  const [listUsers, setListUsers] = useState<UserProps[]>([]);

  return (
    <GlobalContext.Provider
      value={{
        userSearch,
        setUserSearch,
        lastUserSearch,
        setLastUserSearch,
        listUsers,
        setListUsers
      }}
    >
      {props.children}
    </GlobalContext.Provider>
  );
};
